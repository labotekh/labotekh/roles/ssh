# ssh

[![Maintainer](https://img.shields.io/badge/maintained%20by-labotekh-e00000?style=flat-square)](https://gilab.com/labotekh)
[![License](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/labotekh/iac/roles/ssh/-/blob/main/LICENSE)
[![Release](https://gitlab.com/labotekh/iac/roles/ssh/-/badges/release.svg)](https://gitlab.com/labotekh/iac/roles/ssh/-/releases)[![Ansible version](https://img.shields.io/badge/ansible-%3E%3D2.17-black.svg?style=flat-square&logo=ansible)](https://github.com/ansible/ansible)

Set custom SSH port and harden configuration of SSH server

**Platforms Supported**:

| Platform | Versions |
|----------|----------|
| Ubuntu | 24.04.1 |

## ⚠️ Requirements

Ansible >= 2.17.

## ⚡ Installation

### Install with git

If you do not want a global installation, clone it into your `roles_path`.

```bash
git clone https://gitlab.com/labotekh/iac/roles/ssh.git  ssh
```

But I often add it as a submodule in a given `playbook_dir` repository.

```bash
git submodule add https://gitlab.com/labotekh/iac/roles/ssh.git roles/ssh
```

### ✏️ Example Playbook

Basic usage is:

```yaml
- hosts: all
  roles:
    - role: ssh
      vars:
        ansible_host: X.X.X.X
        ansible_port: XXXXX
        root_domain: xxxxxxxxx.xx
        
```

## ⚙️ Role Variables

Variables are divided in three types.

The **default vars** section shows you which variables you may
override in your ansible inventory. As a matter of fact, all variables should
be defined there for explicitness, ease of documentation as well as overall
role manageability.

The **context variables** are shown in section below hint you
on how runtime context may affects role execution.

### Default variables

#### main

Set custom SSH port and harden configuration of SSH server

| Variable Name | Required | Type | Default | Elements | Description |
|---------------|----------|------|---------|----------|-------------|
| root_domain | True | str |  |  | The FQDN of the host |
| ansible_port | True | int |  |  | The custom SSH port to access the host (different from 22) |
| ansible_host | True | str |  |  | The host IP to connect |

## Author Information

Imotekh <imotekh@protonmail.com>